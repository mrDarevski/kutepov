<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('car_types', function( Blueprint $table){
            $table->increments('id');
            $table->string('description');
            $table->string('name');
        });


        Schema::create('cars',function(Blueprint $table){
            $table->increments('id');
            $table->integer('type_id')->unsigned();
            $table->string('model',256);
            $table->string('mark',256);
            $table->date('man_year');
            $table->string('vim',256);
            $table->json('ttx');

            $table->foreign('type_id')->references('id')->on('car_types');
        });

        Schema::create('car_images', function (Blueprint $table){
            $table->increments('id');
            $table->integer('car_id')->unsigned();
            $table->text('url');
            $table->text('description');

            $table->foreign('car_id')->references('id')->on('cars');
        });

        Schema::create('test_drive',function (Blueprint $table){
            $table->increments('id');
            $table->integer('car_id')->unsigned();
            $table->string('name');
            $table->string('description');
            $table->foreign('car_id')->references('id')->on('cars');
        });

        Schema::create('tracks', function (Blueprint $table){
            $table->increments('id');
            $table->string('address',256);
            $table->text('info');
        });

        Schema::create('tests', function (Blueprint $table){
            $table->increments('id');
            $table->integer('test_drive_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('track_id')->unsigned();
            $table->string('name');
            $table->float('max_speed');
            $table->float('avg_speed');
            $table->time('total_time');
            $table->date('test_date');

            $table->foreign('test_drive_id')->references('id')->on('test_drive');
            $table->foreign('track_id')->references('id')->on('tracks');
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('posts', function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('description');
        });

        Schema::create('staff', function (Blueprint $table){
            $table->integer('user_id')->unsigned();
            $table->integer('post_id')->unsigned();

            $table->primary(['user_id','post_id']);

            $table->foreign('post_id')->references('id')->on('posts');
            $table->foreign('user_id')->references('id')->on('users');
        });




        Schema::create('phones', function (Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('number',256);

            $table->foreign('user_id')->references('id')->on('users');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tests', function (Blueprint $table){
            $table->dropForeign(['test_drive_id']);
            $table->dropForeign(['user_id']);
            $table->dropForeign(['track_id']);
        });

        Schema::table('test_drive', function (Blueprint $table){
            $table->dropForeign(['car_id']);
        });

        Schema::table('car_images', function (Blueprint $table){
            $table->dropForeign(['car_id']);
        });

        Schema::table('cars', function (Blueprint $table){
            $table->dropForeign(['type_id']);
        });

        Schema::table('staff', function (Blueprint $table){
            $table->dropForeign(['user_id']);
            $table->dropForeign(['post_id']);
        });
        Schema::table('phones', function (Blueprint $table){
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('cars');
        Schema::dropIfExists('tracks');
        Schema::dropIfExists('phones');
        Schema::dropIfExists('car_types');
        Schema::dropIfExists('tests');
        Schema::dropIfExists('test_drive');
        Schema::dropIfExists('posts');
        Schema::dropIfExists('staff');
        Schema::dropIfExists('car_images');
    }
}
