<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $guarded = ['id'];

    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function testDrive(){
        return $this->belongsTo('App\TestDrive');
    }

    public function track(){
        return $this->belongsTo('App\Tracks');
    }
}
