<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get user phones
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getPhones(){
        return $this->hasMany('App\Phones');
    }

    /**
     * Get all user post in company
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function getUserPosts(){
        return $this->belongsToMany('App\Posts','staff');
    }

}
