<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{
    public $timestamps = false;

    protected $fillable = ['mark','model','man_year','vim','type_id','ttx'];

    public function carImages(){
        return $this->hasMany('App\CarImages','car_id');
    }

    public function type(){
        return $this->belongsTo('App\CarTypes');
    }

    public function testDrive(){
        return $this->hasMany('App\TestDrive','car_id');
    }
}
