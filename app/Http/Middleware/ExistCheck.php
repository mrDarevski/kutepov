<?php

namespace App\Http\Middleware;

use Closure;

class ExistCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $table)
    {
        if (!empty($table::find($request->id)))
            return $next($request);
        else
            abort(404);
    }
}
