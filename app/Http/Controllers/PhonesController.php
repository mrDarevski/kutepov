<?php

namespace App\Http\Controllers;

use App\Phones;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PhonesController extends Controller
{

    /**
     * Show all phones with the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $phones = Auth::user()->getPhones;

        return view('profile.phones',['phones'=>$phones]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request,[
            'phone_number' => 'required',
        ]);

        Phones::create([
            'user_id' => \Auth::user()->id,
            'phone_number'=> $request->input('phone_number'),
        ]);

        return $this->show();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $phoneId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$phoneId)
    {
        $this->validate($request,[
            'phone_number' => 'required'
        ]);

        Phones::find($phoneId)->update($request->all());

        return $this->show();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $phoneId
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function destroy($phoneId)
    {
        Phones::destroy($phoneId);
        return $this->show();
    }
}
