<?php

namespace App\Http\Controllers;

use App\CarTypes;
use Illuminate\Http\Request;

class CarsTypeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @@return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('cars.types_list',['types'=>CarTypes::all()]);
    }

    /**
     * Create a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request,[
            'description'=>'required',
            'name'=>'required',
        ]);

        //dd($request->all());
        CarTypes::create(
            $request->all()
        );

        return $this->show();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$typeId)
    {
        $this->validate($request,[
            'description'=>'required',
            'name'=>'required',
        ]);

        CarTypes::find($typeId)->update($request->all());

        return $this->show();
    }


    public function edit($id)
    {
        $type = CarTypes::find($id);
        return view('cars.types_edit',['type'=>$type]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $typeId
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy($typeId)
    {
        CarTypes::destroy($typeId);
        return $this->show();
    }
}
