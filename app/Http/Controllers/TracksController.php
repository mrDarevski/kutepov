<?php

namespace App\Http\Controllers;

use App\Tracks;
use Illuminate\Http\Request;

class TracksController extends Controller
{

    /**
     * Show all phones with the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $tracks = Tracks::all();

        return view('tracks.list',['tracks'=>$tracks]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $this->validate($request,[
            'address' => 'required|max:256',
            'info' => 'required',
            'name' => 'required',
        ]);

        Tracks::create(
            $request->all()
        );

        return redirect()->action('TracksController@show');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $trackId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$trackId)
    {
        $this->validate($request,[
            'address' => 'required|max:256',
            'info' => 'required',
        ]);

        Tracks::find($trackId)->update($request->all());

        return redirect()->action('TracksController@show');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $trackId
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function destroy($trackId)
    {
        Tracks::destroy($trackId);
        return redirect()->action('TracksController@show');
    }
}
