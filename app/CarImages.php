<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarImages extends Model
{
    protected $guarded = ['id'];

    public $timestamps= false;

}
