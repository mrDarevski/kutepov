<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestDrive extends Model
{
    protected $table = 'test_drive';

    protected $fillable = ['car_id','name','description'];

    public $timestamps = false;

    public function tests(){
        return $this->hasMany('App\Test');
    }
}
