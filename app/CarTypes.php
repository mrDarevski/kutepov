<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarTypes extends Model
{
    protected $table = 'car_types';

    protected $fillable = ['description','name'];

    public $timestamps = false;
}
