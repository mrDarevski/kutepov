@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>
                        Машины: <small>Типы Машин</small>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <ul class="nav nav-stacked nav-pills">
                    <li>
                        <a href="{{url('/cars')}}">Машины</a>
                    </li>
                    <li class=active>
                        <a href="{{url('/cars-type')}}">Типы Машин</a>
                    </li>
                    <li>
                        <a href="{{url('')}}">Работники</a>
                    </li>
                    <li>
                        <a href="{{url('/tracks')}}">Треки</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-10">


                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Тип Машины</th>
                        <th>Описание</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($types as $type)
                        <tr>
                            <td>{{$type->type_id}}</td>
                            <td>{{$type->name}}</td>
                            <td>{{$type->description}}</td>
                            <td><a href="{{'/cars-type/destroy/'.$type->type_id}}"><span class="glyphicon glyphicon-trash" aria-hidden="true"> </span></a><a href="{{'/cars-type/edit/'.$type->type_id}}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <form role="form" method="post" action="/cars-type/create">
                    {{csrf_field()}}
                    <div class="form-group">

                        <label for="exampleInputEmail1">
                            Название типа
                        </label>
                        <input name="name" type="text" class="form-control" id="exampleInputEmail1" />
                    </div>
                    <div class="form-group">

                        <label for="exampleInputPassword1">
                            Описание типа
                        </label>
                        <input name="description" type="text" class="form-control" id="exampleInputPassword1" />
                    </div>

                    <button type="submit" class="btn btn-default">
                        Добавить тип машины
                    </button>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection