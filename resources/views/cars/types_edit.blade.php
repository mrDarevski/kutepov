@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>
                        Редактирование типа автомобиля: <small> {{$type->name}}</small>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <ul class="nav nav-stacked nav-pills">
                    <li>
                        <a href="{{url('/cars')}}">Машины</a>
                    </li>
                    <li class=active>
                        <a href="{{url('/cars-type')}}">Типы Машин</a>
                    </li>
                    <li>
                        <a href="{{url('')}}">Работники</a>
                    </li>
                    <li>
                        <a href="{{url('/tracks')}}">Треки</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-10">
                <form role="form" method="post" action="/cars-type/update/{{$type->type_id}}">
                    {{csrf_field()}}
                    <div class="form-group">

                        <label for="exampleInputEmail1">
                            Название типа
                        </label>
                        <input name="name" type="text" value="{{$type->name}}" class="form-control" id="exampleInputEmail1" />
                    </div>
                    <div class="form-group">

                        <label for="exampleInputPassword1">
                            Описание типа
                        </label>
                        <input name="description" type="text" value="{{$type->description}}" class="form-control" id="exampleInputPassword1" />
                    </div>

                    <button type="submit" class="btn btn-default">
                        Обновить данные
                    </button>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection