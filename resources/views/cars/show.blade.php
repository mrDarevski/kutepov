@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1 style="display: inline">
                        Машина: <small>{{$car->mark.' '.$car->model}}</small>
                    </h1>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <ul class="nav nav-stacked nav-pills">
                    <li class=active >
                        <a href="{{url('/cars')}}">Машины</a>
                    </li>
                    <li>
                        <a href="{{url('/cars-type')}}">Типы Машин</a>
                    </li>
                    <li>
                        <a href="{{url('')}}">Работники</a>
                    </li>
                    <li>
                        <a href="{{url('/tracks')}}">Треки</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-2">
            </div>

            <div class="col-md-6">
                @if (!empty($car->images->toArray()))
                <div class="carousel slide" id="carousel-117616">
                    <div class="carousel-inner">
                        @foreach($car->images as $image)
                            <div class="item {{($image == $car->images[0]) ? 'active' : ''}}">
                                <img alt="Carousel Bootstrap First" src="{{'/images/thumb/'.$image->url}}" />

                                <div class="carousel-caption">
                                    <p>
                                        {{$image->description}}
                                    </p>
                                </div>
                            </div>

                        @endforeach

                    </div> <a class="left carousel-control" href="#carousel-117616" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-117616" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
                    @else
                        <h1>Изображения отсутсвуют</h1>
                @endif
                    <dl class="dl-horizontal">
                        <dt >
                            Марка
                        </dt>
                        <dd>
                            {{$car->mark}}
                        </dd>
                        <dt>
                            Модель
                        </dt>
                        <dd>
                            {{$car->model}}
                        </dd>
                        <dt>
                            Год выпуска
                        </dt>
                        <dd>
                            {{$car->man_year}}
                        </dd>
                        <dt>
                            Тип Авто
                        </dt>
                        <dd>
                            {{$car->getCarType->name}}
                        </dd>
                    </dl>
                    <a href="{{url('/cars/destroy/'.$car->car_id)}}" class="btn btn-default btn-danger" type="Просмотреть тесты">Удалить машину</a>
                    <a href="{{url('/cars/get-tests/'.$car->car_id)}}" class="btn btn-default btn-group-sm" type="Просмотреть тесты">Просмотреть тесты</a>
            </div>

            <div class="col-md-2">
            </div>
        </div>
    </div>
@endsection