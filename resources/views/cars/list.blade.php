@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1 style="display: inline">
                        Машины: <small>Список машин</small>
                    </h1>
                    <a href="{{url('/cars/create')}}" class="btn btn-default btn-primary" type="Добавить машину">Добавить машину</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <ul class="nav nav-stacked nav-pills">
                    <li class=active >
                        <a href="{{url('/cars')}}">Машины</a>
                    </li>
                    <li>
                        <a href="{{url('/cars-type')}}">Типы Машин</a>
                    </li>
                    <li>
                        <a href="{{url('')}}">Работники</a>
                    </li>
                    <li>
                        <a href="{{url('/tracks')}}">Треки</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-10">
                <div class="row">

                    @foreach($cars as $car)
                        <div class="col-md-6 text-center" style="margin-bottom: 20px;">
                            <img style= "width:100%; height: auto; object-fit: contain; max-height: 200px;" alt="Bootstrap Image Preview" src="{{ (!empty($car['images'][0])) ? '/images/thumb/'.($car['images'][0]->url) : 'http://placehold.it/600x300'}}"/>
                            <dl class="dl-horizontal">
                                <dt >
                                    Марка
                                </dt>
                                <dd>
                                    {{$car->mark}}
                                </dd>
                                <dt>
                                    Модель
                                </dt>
                                <dd>
                                    {{$car->model}}
                                </dd>
                                <dt>
                                    Год выпуска
                                </dt>
                                <dd>
                                    {{$car->man_year}}
                                </dd>
                                <dt>
                                    Тип Авто
                                </dt>
                                <dd>
                                    {{$car->getCarType->name}}
                                </dd>
                            </dl>
                            <a href="{{url('/cars/get-tests/'.$car->car_id)}}" class="btn btn-default btn-group-sm" type="Просмотреть тесты">Просмотреть тесты</a>
                            <a href="{{url('/cars/show/'.$car->car_id)}}" class="btn btn-default btn-group-sm" type="Просмотреть тесты">Просмотреть информацию</a>
                        </div>

                    @endforeach

                </div>
                {!! $cars->render() !!}
            </div>




        </div>


    </div>




@endsection