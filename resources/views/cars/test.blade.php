@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>
                        Тесты: <small>Авто - {!! $car->mark.' '.$car->model !!}</small>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <ul class="nav nav-stacked nav-pills">
                    <li>
                        <a href="{{url('/cars')}}">Машины</a>
                    </li>
                    <li>
                        <a href="{{url('/cars-type')}}">Типы Машин</a>
                    </li>
                    <li>
                        <a href="{{url('')}}">Работники</a>
                    </li>
                    <li>
                        <a href="{{url('/tracks')}}">Треки</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-10">



                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Название трека</th>
                        <th>Максимальная скорость</th>
                        <th>Средняя скорость</th>
                        <th>Время</th>
                        <th>Дата</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tests as $test)
                        <tr>
                            <td>{{$test->id}}</td>
                            <td>{{$test->name}}</td>
                            <td>{{$test->max_speed}}</td>
                            <td>{{$test->avg_speed}}</td>
                            <td>{{$test->time}}</td>
                            <td>{{$test->date}}</td>
                        </tr>
                    @endforeach

                    <tr>
                        <td></td>
                        <td></td>
                        <td style="color: red">Максимальаня скорость {{$maxSpeed}}</td>
                        <td style="color: red">Средняя скорость - {{$avgSpeed}}</td>
                        <td></td>
                        <td></td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
@endsection