@extends('layouts.app')

@section('content')
    {!! var_dump($errors) !!}
    {!! Form::open(['action'=> 'CarsController@store', 'method' => 'post','files' => true]) !!}
        {{ Form::text('mark') }}
        {{ Form::text('model') }}
        {{ Form::text('man_year') }}
        {{ Form::select('car_type_id', array('1' => 'Large', '2' => 'Small')) }}
        {{ Form::submit('Click Me!') }}
        {{ Form::file('files[]',['multiple']) }}
    {!! Form::close() !!}

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1 style="display: inline">
                        Машины: <small>Добавление автомобиля</small>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <ul class="nav nav-stacked nav-pills">
                    <li>
                        <a href="{{url('/cars')}}">Машины</a>
                    </li>
                    <li>
                        <a href="{{url('/cars-type')}}">Типы Машин</a>
                    </li>
                    <li>
                        <a href="{{url('')}}">Работники</a>
                    </li>
                    <li>
                        <a href="{{url('/tracks')}}">Треки</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-10">
                {!! Form::open(['action'=> 'CarsController@store', 'method' => 'post','files' => true]) !!}
                    <div class="form-group">

                        <label for="exampleInputEmail1">
                            Модель
                        </label>
                        <input type="text" name="model" class="form-control" id="exampleInputEmail1" />
                    </div>
                    <div class="form-group">

                        <label for="exampleInputPassword1">
                            Марка
                        </label>
                        <input type="password" name="mark" class="form-control" id="exampleInputPassword1" />
                    </div>

                    <div class="form-group">

                        <label for="exampleInputPassword2">
                            Дата выпуска
                        </label>
                        <input type="date" name="man_year" class="form-control" id="exampleInputPassword2" />
                    </div>
                    <div class="form-group">

                        <label for="exampleInputFile">
                            Изображения
                        </label>
                        <input type="file" name="files[]" id="exampleInputFile" multiple/>
                        <p class="help-block">
                            Загрузите изображение автомобиля
                        </p>
                    </div>

                    <div class="form-group">

                        <label for="exampleInputFile">
                            Тип автомобиля
                        </label>
                        {{ Form::select('car_type_id', $types) }}
                    </div>

                    <button type="submit" class="btn btn-default">
                        Submit
                    </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection