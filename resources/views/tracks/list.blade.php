@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>
                        Треки: <small>Список треков</small>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <ul class="nav nav-stacked nav-pills">
                    <li>
                        <a href="{{url('/cars')}}">Машины</a>
                    </li>
                    <li >
                        <a href="{{url('/cars-type')}}">Типы Машин</a>
                    </li>
                    <li>
                        <a href="{{url('')}}">Работники</a>
                    </li>
                    <li class=active>
                        <a href="{{url('/tracks')}}">Треки</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-10">


                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Название трека</th>
                        <th>Адрес</th>
                        <th>Описание</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tracks as $track)
                        <tr>
                            <td>{{$track->track_id}}</td>
                            <td>{{$track->name}}</td>
                            <td>{{$track->address}}</td>
                            <td>{{$track->info}}</td>
                            <td><span class="glyphicon glyphicon-trash" aria-hidden="true"></span><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <form role="form" method="post" action="{{url('/tracks/create')}}">


                    {{csrf_field()}}
                    <div class="form-group">

                        <label for="exampleInputEmail1">
                            Название трека
                        </label>
                        <input name="name" type="text" class="form-control" id="exampleInputEmail1" />
                    </div>
                    <div class="form-group">

                        <label for="exampleInputPassword1">
                            Адрес
                        </label>
                        <input type="text" name="address" class="form-control" id="exampleInputPassword1" />
                    </div>

                    <div class="form-group">

                        <label for="exampleInputPassword2">
                            Описание Трассы
                        </label>
                        <input type="text" name="info" class="form-control" id="exampleInputPassword2" />
                    </div>

                    <button type="submit" class="btn btn-default">
                        Добавить трассу
                    </button>
                </form>
            </div>
        </div>
@endsection
