<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Cars;
Route::get('/', 'CarsController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');



Route::group(['middleware'=>'auth'],function (){

    // User Phones Func
    Route::get('/phones/', 'PhonesController@show');
    Route::get('/phones/create', 'PhonesController@create');
    Route::group(['middleware'=>'phone_check'],function (){
        Route::post('/phones/update/{id}', 'PhonesController@update')->where('id','[0-9]+');
        Route::post('/phones/destroy/{id}', 'PhonesController@destroy')->where('id','[0-9]+');
    });


});



// Cars Types
Route::get('/cars-type/', 'CarsTypeController@show');

// Cars
Route::get('/cars/', 'CarsController@index');
Route::get('/cars/get-tests/{id}', 'CarsController@getTests')->where('id','[0-9]+')->middleware('exist_check:\App\Cars');

Route::get('/tracks/', 'TracksController@show');

Route::group(['middleware'=>['admin']],function (){
    Route::post('/cars-type/create', 'CarsTypeController@create');

    Route::group(['middleware'=>'existCheck:\App\CarTypes'],function (){
        Route::any('/cars-type/edit/{id}', 'CarsTypeController@edit')->where('id','[0-9]+');
        Route::any('/cars-type/update/{id}', 'CarsTypeController@update')->where('id','[0-9]+');
        Route::any('/cars-type/destroy/{id}', 'CarsTypeController@destroy')->where('id','[0-9]+');
    });


    Route::post('/tracks/create', 'TracksController@create');
    Route::group(['middleware'=>'existCheck:\App\Tracks'],function (){
        Route::any('/tracks/update/{id}', 'TracksController@update')->where('id','[0-9]+');
        Route::any('/tracks/destroy/{id}', 'TracksController@destroy')->where('id','[0-9]+');
    });


    Route::get('/cars/create', 'CarsController@create');
    Route::post('/cars/store', 'CarsController@store');

    Route::group(['middleware'=>'exist_check:\App\Cars'],function (){
        Route::get('/cars/show/{id}', 'CarsController@show')->where('id','[0-9]+');
        Route::get('/cars/edit/{id}', 'CarsController@edit')->where('id','[0-9]+');
        Route::post('/cars/update/{id}', 'CarsController@update')->where('id','[0-9]+');
        Route::post('/cars/deleteImage/{id}', 'CarsController@deleteImage')->where('id','[0-9]+');
        Route::get('/cars/destroy/{id}', 'CarsController@destroy')->where('id','[0-9]+');

        Route::get('/cars/add-test/{id}', 'CarsController@addTestForm')->where('id','[0-9]+');
        Route::get('/cars/tests/{id}', 'CarsController@getTests')->where('id','[0-9]+');
        Route::post('/cars/save-test/{id}', 'CarsController@createTest')->where('id','[0-9]+');
    });


});



